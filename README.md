

How to install


install virtual enviroment if not have installed
`python3 -m pip install virtualenv`

create virtual environment
`python3 -m venv nombre_de_tu_espacio_virtual`

activate virtual environment windows
`nombre_de_tu_espacio_virtual\Scripts\activate`

activate virtual environment linux
`source nombre_de_tu_espacio_virtual/bin/activate`


install requirements
`pip install -r requirements.txt`

make migrations with 
`python3 manage.py makemigrations appname`

and
`python3 manage.py migrate`

---

# Start

start server on localhost
`python3 manage.py runserver`




