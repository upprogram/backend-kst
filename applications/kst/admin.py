from django.contrib import admin
from .models import KSTModel, AreaModel, TestModel, ResultModel, QuestionModel, LevelModel, MilestoneModel

admin.site.register(KSTModel)
admin.site.register(TestModel)
admin.site.register(ResultModel)
admin.site.register(QuestionModel)
admin.site.register(AreaModel)
admin.site.register(LevelModel)
admin.site.register(MilestoneModel)

