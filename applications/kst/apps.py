from django.apps import AppConfig


class KstConfig(AppConfig):
    name = 'applications.kst'
