from django.db import models
from applications.student.models import StudentModel


class AreaModel(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Area'
        verbose_name_plural = 'Areas'
        db_table = 'areas'

    def __str__(self):
        return self.name


class KSTModel(models.Model):
    user = models.ForeignKey(StudentModel, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    area = models.ForeignKey(AreaModel, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'KST'
        verbose_name_plural = 'KSTs'
        db_table = 'kst'

    def __str__(self):
        return self.user.name
    
class LevelModel(models.Model):
    level = models.CharField(max_length=100)
    class Meta:
        verbose_name = 'Level'
        verbose_name_plural = 'Levels'
        db_table = 'levels'

    def __str__(self):
        return self.level
    
class MilestoneModel(models.Model):
    milestone = models.CharField(max_length=100)
    class Meta:
        verbose_name = 'Milestone'
        verbose_name_plural = 'Milestones'
        db_table = 'milestones'

    def __str__(self):
        return self.milestone

class QuestionModel(models.Model):
    level = models.ForeignKey(LevelModel, on_delete=models.DO_NOTHING)
    milestone = models.ForeignKey(MilestoneModel, on_delete=models.DO_NOTHING)
    mastery = models.CharField(max_length=100)
    kst = models.CharField(max_length=100)
    

    class Meta:
        verbose_name = 'Question'
        verbose_name_plural = 'Questions'
        db_table = 'questions'

    def __str__(self):
        return str(self.kst)


class ResultModel(models.Model):
    question = models.ForeignKey(QuestionModel, on_delete=models.PROTECT)
    kst = models.ForeignKey(KSTModel, on_delete=models.PROTECT)
    mastery_level = models.IntegerField(default=3)
    student_interview = models.CharField(max_length=250, default="")

    class Meta:
        verbose_name = 'Result'
        verbose_name_plural = 'Results'
        db_table = 'results'

    def __str__(self):
        return self.question


class TestModel(models.Model):
    area = models.ForeignKey(AreaModel, on_delete=models.PROTECT)
    question = models.ManyToManyField(QuestionModel)

    class Meta:
        verbose_name = 'Test'
        verbose_name_plural = 'Tests'
        db_table = 'tests'

    def __str__(self):
        return str(self.pk)+ ' ' +str(self.area)
