﻿from rest_framework import serializers, pagination
from .models import KSTModel, AreaModel, ResultModel, QuestionModel, TestModel

class KSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = KSTModel
        fields = '__all__'
        

class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaModel
        fields = '__all__'
        
class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResultModel
        fields = '__all__'
        
class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionModel
        fields = '__all__'
        
class QuestionPagination(pagination.PageNumberPagination):
    page_size = 20
    max_page_size = 100
    
class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestModel
        fields = '__all__'