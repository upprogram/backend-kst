﻿from django.urls import path

#local
from . import views

app_name="kst_app"

urlpatterns = [
    path('api/new-kst/', views.KSTCreateView.as_view(),),
    path('api/new-area/', views.CreateAreaView.as_view(),),
    path('api/list-areas/', views.ListAreasView.as_view(),),
    path('api/update-area/', views.UpdateAreasView.as_view(),),
    path('api/delete-area/', views.DeleteAreasView.as_view(),),
    path('api/new-question/', views.CreateQuestionsView.as_view(),),
    path('api/list-question/', views.ListQuestionsView.as_view(),),
    path('api/new-test/', views.CreateTestView.as_view(),),
]