from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    RetrieveUpdateAPIView,
)

from .models import KSTModel, TestModel, ResultModel, AreaModel, QuestionModel
from .serializers import KSTSerializer, TestSerializer, ResultSerializer, AreaSerializer, QuestionSerializer, QuestionPagination




#Create a new KST
class KSTCreateView(CreateAPIView):
    serializer_class = KSTSerializer


#Questions with pagination of 20 questions

#GET areas
class ListAreasView(ListAPIView):
    serializer_class = AreaSerializer
    
    def get_queryset(self):
        return AreaModel.objects.all()
    
#Create new areas 
class CreateAreaView(CreateAPIView):
    serializer_class = AreaSerializer
    
#Modify areas 
class UpdateAreasView(RetrieveUpdateAPIView):
    serializer_class = AreaSerializer
    queryset = AreaModel.objects.all()

#Delete areas 
class DeleteAreasView(DestroyAPIView):
    serializer_class = AreaSerializer
    queryset = AreaModel.objects.all()

#Create new Questions 
class CreateQuestionsView(CreateAPIView):
    serializer_class = QuestionSerializer
    

#get questions pag 20 
class ListQuestionsView(ListAPIView):
    serializer_class = QuestionSerializer
    pagination_class = QuestionPagination
    
    def get_queryset(self):
        return QuestionModel.objects.all()
    
#create new test 
class CreateTestView(CreateAPIView):
    serializer_class = TestSerializer

#post answers 

#Download or send email report

#Save report to student

#CRUD Questions and areas

#Get users

#Get student by cohort 

#Get student by Areas

#Get student by country

#Get Report total prod, by area domain total users

#Get Report total prod, by area domain of vertical by cohort



