from django.apps import AppConfig


class StudentConfig(AppConfig):
    name = 'applications.student'
