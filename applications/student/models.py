from django.db import models
from applications.users.models import User


COHORT_CHOICES = [
    ('C1', 'Cohort 1'),
    ('C2', 'Cohort 2'),
    ('C3', 'Cohort 3'),
    ('C4', 'Cohort 4'),
    ('C5', 'Cohort 5'),
    ('C6', 'Cohort 6'),
    ('C7', 'Cohort 7'),
    ('C8', 'Cohort 8'),
]

class StudentModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cohort = models.CharField(max_length=20, choices=COHORT_CHOICES)
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    register_date = models.DateTimeField(auto_now_add=True)
    last_connection = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'
        db_table = 'students'

    def __str__(self):
        return str(self.name) + ' ' + str(self.last_name) + ' - ' + str(self.user)