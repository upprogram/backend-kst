﻿from django.urls import path

#local
from . import views

app_name="student_app"

urlpatterns = [
    path('api/create-student/', views.StudentCreateView.as_view(),),
]