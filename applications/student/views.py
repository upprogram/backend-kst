from rest_framework.generics import CreateAPIView

from .models import StudentModel
from .serializers import StudentSerializer



#Post api data for complete user
class StudentCreateView(CreateAPIView):
    serializer_class = StudentSerializer
    