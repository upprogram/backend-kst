from django.db import models

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
#
from .managers import UserManager
#

class User(AbstractBaseUser, PermissionsMixin):
    #
    email = models.EmailField(unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    #
    USERNAME_FIELD = 'email'
    #
    objects = UserManager()

    def get_short_name(self):
        return self.email