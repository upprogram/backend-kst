﻿from django.urls import path

#local
from . import views

app_name="users_app"

urlpatterns = [
    path('api/google-login/', views.GoogleLoginView.as_view(),),
    path('api/register/', views.RegistrationView.as_view()),
    path('api/users/', views.ListUsersView.as_view()),
]