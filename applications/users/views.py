from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from rest_framework import status
# third party
from firebase_admin import auth
from rest_framework.views import APIView
# serializers
from .serializers import LoginSocialSerializer, RegistrationsSerializer, AdminUserTokenSerializer
# models
from .models import User

#Login with Google
class GoogleLoginView(APIView):
    serializer_class = LoginSocialSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        #
        id_token = serializer.data.get('token_id')
        #
        decoded_token = auth.verify_id_token(id_token)
        #
        email = decoded_token['email']
        name = decoded_token['name']
        avatar = decoded_token['picture']
        #
        usuario, created = User.objects.get_or_create(
            email=email,
            defaults={
                'email': email,
                'is_active': True
            }
        )
        #
        if created:
            token = Token.objects.create(user=usuario)
        else:
            token = Token.objects.get(user=usuario)

        userGet = {
            'id': usuario.pk,
            'email': usuario.email,
        }
        return Response(
            {
                'token': token.key,
                'user': userGet
            }
        )

#User Register with email and password
class RegistrationView(APIView):
    serializer_class = RegistrationsSerializer

    def post(self, request):
        serializer = RegistrationsSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            data['response'] = "successfully registered a new user."
            data['id'] = account.pk
            data['email'] = account.email
        else:
            data = serializer.errors
        return Response(data)


#Get Users 
class ListUsersView(ListAPIView):
    serializer_class = AdminUserTokenSerializer

    def get_queryset(self):
        return User.objects.all()